package android.tannera.to_do;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class ToDoListFragment extends Fragment {

    private RecyclerView recyclerView;
    private MainActivity activity;
    private ActivityCallback activityCallback;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        ToDoPost ToDoItem1 = new ToDoPost("Post 1", "2","3","4");
        ToDoPost ToDoItem2 = new ToDoPost("Post 2", "2", "3", "4");
        ToDoPost ToDoItem3 = new ToDoPost("Post 3", "2", "3","4");
        ToDoPost ToDoItem4 = new ToDoPost("Post 4", "2", "3", "4");

        activity.todoItems.add(ToDoItem1);
        activity.todoItems.add(ToDoItem2);
        activity.todoItems.add(ToDoItem3);
        activity.todoItems.add(ToDoItem4);



        ToDoPostAdapter adapter = new ToDoPostAdapter(activity.todoItems, activityCallback);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
