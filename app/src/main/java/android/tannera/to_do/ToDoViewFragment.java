package android.tannera.to_do;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ToDoViewFragment extends Fragment{

    private RecyclerView recyclerView;
    private MainActivity activity;
    private ActivityCallback activityCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.view_fragment, container, false);

        TextView tv1 = (TextView)layoutView.findViewById(R.id.view1);
        TextView tv2 = (TextView)layoutView.findViewById(R.id.view2);
        TextView tv3 = (TextView)layoutView.findViewById(R.id.view3);
        TextView tv4 = (TextView)layoutView.findViewById(R.id.view4);

        tv1.setText("Name: " + activity.todoItems.get(activity.currentItem).title);
        tv2.setText("Name: " + activity.todoItems.get(activity.currentItem).dateAdded);
        tv3.setText("Name: " + activity.todoItems.get(activity.currentItem).dateDue);
        tv4.setText("Name: " + activity.todoItems.get(activity.currentItem).category);

        return layoutView;
    }
}
