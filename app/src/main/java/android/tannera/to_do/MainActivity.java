package android.tannera.to_do;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback{

    public ArrayList<ToDoPost> todoItems = new ArrayList<ToDoPost>();
    public int currentItem;

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();
    }


    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ToDoViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
