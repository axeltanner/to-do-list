package android.tannera.to_do;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoPostAdapter extends RecyclerView.Adapter<ToDoPostHolder>{


    private ArrayList<ToDoPost> todoItems;
    private ActivityCallback activityCallback;

    public ToDoPostAdapter(ArrayList<ToDoPost> todoItems, ActivityCallback activityCallback){
        this.todoItems = todoItems;
        this.activityCallback = activityCallback;
    }


    @Override
    public ToDoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallback.onPostSelected(position);
            }
        });

        holder.titleText.setText(todoItems.get(position).title);
    }

    @Override
    public int getItemCount() {
        return todoItems.size();
    }
}
